# Examples

All examples are in the source repository, some are also shown here for easy accessibility.

## device.py
```py linenums="1" title="device.py"
--8<-- "../examples/device.py"
```

## amen.py
```py linenums="1" title="amen.py"
--8<-- "../examples/amen.py"
```

## calibrate_latency.py
```python linenums="1" title="calibrate_latency.py"
--8<-- "../examples/calibrate_latency.py"
```

## calibrate_voltage.py
```python linenums="1" title="calibrate_voltage.py"
--8<-- "../examples/calibrate_voltage.py"
```

## calibrate_pressure.py
```python linenums="1" title="calibrate_pressure.py"
--8<-- "../examples/calibrate_pressure.py"
```

## plot_gainramp.py
```python linenums="1" title="plot_gainramp.py"
--8<-- "../examples/plot_gainramp.py"
```
The result should look like this:

![Gain Ramp](imgs/plot_gainramp.png)

## profiling.py
```python linenums="1" title="profiling.py"
--8<-- "../examples/profiling.py"
```